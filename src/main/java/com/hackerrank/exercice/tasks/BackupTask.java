package com.hackerrank.exercice.tasks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import com.hackerrank.exercice.model.backup.BackupDTO;
import com.hackerrank.exercice.services.PersistenceService;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class BackupTask implements Runnable {

  private static final Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().serializeNulls()
      .create();

  private static final String INDENT = "  ";

  @Override
  public void run() {
    HashMap<String, ArrayList<String>> allPaths = PersistenceService.INSTANCE.getAllPaths();
    List<BackupDTO> dtos = convertToBackupDTO(allPaths);
    writeBackup(dtos);
  }

  private List<BackupDTO> convertToBackupDTO(HashMap<String, ArrayList<String>> allPaths) {
    List<BackupDTO> out = new ArrayList<>();
    for (Entry<String, ArrayList<String>> entry : allPaths.entrySet()) {
      out.add(new BackupDTO(entry.getKey(), entry.getValue()));
    }
    return out;
  }

  public static void writeBackup(List<BackupDTO> dtos) {
    for (BackupDTO dto : dtos) {
      try (JsonWriter writer = new JsonWriter(
          new FileWriter("backup.json"))) {
        writer.setIndent(INDENT);
        gsonBuilder.toJson(dto, BackupDTO.class, writer);
      } catch (IOException e) {
        //Todo pendiente manejo de errores
      }
    }
  }
}
