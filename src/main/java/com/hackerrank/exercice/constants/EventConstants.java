package com.hackerrank.exercice.constants;

public class EventConstants {

  public static final String LS_R = "ls -r";

  public static final String LS = "ls";
}
