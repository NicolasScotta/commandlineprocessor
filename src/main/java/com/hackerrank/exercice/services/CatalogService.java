package com.hackerrank.exercice.services;

import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.model.events.CDEvent;
import com.hackerrank.exercice.model.events.ConsoleEvent;
import com.hackerrank.exercice.model.events.LSEvent;
import com.hackerrank.exercice.model.events.MKDirEvent;
import com.hackerrank.exercice.model.events.PWDEvent;
import com.hackerrank.exercice.model.events.QuitEvent;
import com.hackerrank.exercice.model.events.TouchEvent;
import com.hackerrank.exercice.model.events.UnrecognizedEvent;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public enum CatalogService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  private List<ConsoleEvent> registeredEvents;

  CatalogService() {
    this.registeredEvents = Arrays.asList(
        new LSEvent(),
        new CDEvent(),
        new PWDEvent(),
        new MKDirEvent(),
        new TouchEvent(),
        new QuitEvent(),
        new UnrecognizedEvent());
  }


  public ResultDTO processLine(String line) {
    ResultDTO output = null;
    Iterator<ConsoleEvent> it = registeredEvents.iterator();
    while (it.hasNext() && output == null) {
      ConsoleEvent processor = it.next();
      output = processor.processEvent(line);
    }
    return output;
  }
}
