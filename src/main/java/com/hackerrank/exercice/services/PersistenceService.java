package com.hackerrank.exercice.services;

import com.hackerrank.exercice.dao.ComandDao;
import com.hackerrank.exercice.model.dto.ResultDTO;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum PersistenceService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  private ComandDao commandDao;

  PersistenceService() {
    commandDao = new ComandDao();
  }

  public void persist(String baseDir, String command, ResultDTO resultDTO) {
    if (resultDTO.isShouldPersist()) {
      if (!resultDTO.isFile()) {
        String[] s = command.split(" ");
        commandDao.mkdir(baseDir, s[1]);
        BackupService.INSTANCE.backupMemoryPaths();
      } else {
        //FIXME To be implemented
      }
    }
  }

  public List<String> ls(String path) {
    return commandDao.lsWithoutParent(path);
  }

  public List<String> lsRecursively(String path) {
    ArrayList<String> output = new ArrayList<>();
    commandDao.lsRecursively(output, path);
    return output;
  }

  public boolean checkExistsDirectory(String path) {
    File file = new File(path);
    return (file.exists() && file.isDirectory())
        || commandDao.existsDirectory(ContextualPathService.INSTANCE.getBasePath(), path);
  }

  public HashMap<String, ArrayList<String>> getAllPaths() {
    return commandDao.getAllPaths();
  }

  public void setAllPaths(HashMap<String, ArrayList<String>> paths) {
    commandDao.setAllPaths(paths);
  }

}
