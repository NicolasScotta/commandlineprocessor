package com.hackerrank.exercice.services;


import com.hackerrank.exercice.model.dto.ResultDTO;

public enum WriterService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  public void write(ResultDTO resultDTO) {
    for (String line : resultDTO.getOutputLines()) {
      writeNewLine(line);
    }
    writePath();
  }

  public void writePath() {
    writeInLine(ContextualPathService.INSTANCE.getBasePath() + ">");
  }

  public void writeInLine(String line) {
    System.out.print(line);
  }

  public void writeNewLine(String line) {
    System.out.println(line);
  }
}
