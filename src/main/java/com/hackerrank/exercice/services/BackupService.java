package com.hackerrank.exercice.services;

import com.hackerrank.exercice.tasks.BackupTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public enum BackupService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  private ExecutorService executorService = Executors.newSingleThreadExecutor();

  public void backupMemoryPaths() {
    BackupTask backupTask = new BackupTask();
    executorService.submit(backupTask);
  }

  public void shutdown() {
    executorService.shutdown();
  }
}
