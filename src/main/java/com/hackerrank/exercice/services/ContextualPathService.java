package com.hackerrank.exercice.services;

import com.hackerrank.exercice.dao.ContextualPathDao;
import com.hackerrank.exercice.model.dto.ResultDTO;
import java.io.File;

public enum ContextualPathService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  private ContextualPathDao contextualPathDao;

  ContextualPathService() {
    contextualPathDao = new ContextualPathDao();
  }

  public void changeDir(String baseDir, String command, ResultDTO resultDTO) {
    if (resultDTO.isChangeDir()) {
      String[] s = command.split(" ");
      if (!s[1].startsWith("C:") && !s[1].startsWith(File.separator)) {
        contextualPathDao.setPath(baseDir + File.separator + s[1]);
      } else {
        contextualPathDao.setPath(s[1]);
      }

    } else if (resultDTO.isParentDir()) {
      contextualPathDao.changeToParent();
    } else if (resultDTO.isRoot()) {
      contextualPathDao.changeToRoot();
    }
  }

  public String getBasePath() {
    return contextualPathDao.getPath();
  }

  private boolean isRootPath() {
    return contextualPathDao.getPath().indexOf(File.separator) == 0;
  }

  private void changeToParent() {
    contextualPathDao.changeToParent();
  }
}
