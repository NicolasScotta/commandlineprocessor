package com.hackerrank.exercice.services;


import com.hackerrank.exercice.model.dto.ResultDTO;

public enum OrchestratorService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  public boolean processInputCommand(String command) {
    ResultDTO resultDTO = CatalogService.INSTANCE.processLine(
        command);
    PersistenceService.INSTANCE.persist(
        ContextualPathService.INSTANCE.getBasePath(), command,
        resultDTO);
    ContextualPathService.INSTANCE.changeDir(
        ContextualPathService.INSTANCE.getBasePath(), command,
        resultDTO);
    WriterService.INSTANCE.write(resultDTO);
    return resultDTO.isShouldExit();
  }

  public void writePath() {
    WriterService.INSTANCE.writeInLine(
        ContextualPathService.INSTANCE.getBasePath() + ">");
  }
}
