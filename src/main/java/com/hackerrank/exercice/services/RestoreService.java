package com.hackerrank.exercice.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.hackerrank.exercice.model.backup.BackupDTO;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum RestoreService {
  /**
   * Instancia de Singleton del Servicio
   */
  INSTANCE;

  private static final Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().serializeNulls()
      .create();

  public void restoreMemoryPaths() {
    List<BackupDTO> backupDTOS = readBackup();
    HashMap<String, ArrayList<String>> map = convertToMapStorage(backupDTOS);
    PersistenceService.INSTANCE.setAllPaths(map);
  }

  private HashMap<String, ArrayList<String>> convertToMapStorage(List<BackupDTO> backupDTOS) {
    HashMap<String, ArrayList<String>> map = new HashMap<>();
    for (BackupDTO backupDTO : backupDTOS) {
      map.put(backupDTO.getRoot(), new ArrayList<>(backupDTO.getDirectories()));
    }
    return map;
  }

  public static List<BackupDTO> readBackup() {
    List<BackupDTO> backups = new ArrayList<BackupDTO>();
    File file = new File("backup.json");
    if (file.exists()) {
      try (JsonReader reader = new JsonReader(new FileReader(file))) {
        BackupDTO backup = gsonBuilder.fromJson(reader, BackupDTO.class);
        backups.add(backup);
      } catch (IOException e) {
        //Todo pendiente manejo de errores
      }
    }
    return backups;
  }
}
