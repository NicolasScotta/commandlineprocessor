package com.hackerrank.exercice.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ComandDao {

  public static final String SUFIX_MEMORY_FILE = " <-- Memory File";
  private HashMap<String, ArrayList<String>> dir;

  public ComandDao() {
    this.dir = new HashMap<>();
  }

  public void mkdir(String baseDirectory, String newDirectory) {
    if (dir.containsKey(baseDirectory)) {
      ArrayList<String> strings = dir.get(baseDirectory);
      strings.add(baseDirectory + File.separator + newDirectory + SUFIX_MEMORY_FILE);
    } else {
      dir.put(baseDirectory, new ArrayList<>(
          Arrays.asList(baseDirectory + File.separator + newDirectory + SUFIX_MEMORY_FILE)));
    }
  }

  public List<String> ls(String baseDirectory) {
    return dir.getOrDefault(baseDirectory, new ArrayList<>());
  }

  public List<String> lsWithoutParent(String baseDirectory) {
    return dir.getOrDefault(baseDirectory, new ArrayList<>()).stream()
        .map(s -> s.replace(baseDirectory, "")).collect(Collectors.toList());
  }

  public List<String> lsRecursively(ArrayList<String> result, String path) {
    String cleanPath = path.replace(SUFIX_MEMORY_FILE, "");
    ArrayList<String> a = dir.getOrDefault(cleanPath, new ArrayList<>());
    for (String s : a) {
      result.add(s);
      result.addAll(lsRecursively(new ArrayList<>(), s));
    }
    return result;
  }

  public boolean existsDirectory(String baseDirectory, String path) {
    List<String> ls = ls(baseDirectory);
    return ls.contains(path + SUFIX_MEMORY_FILE);
  }

  public HashMap<String, ArrayList<String>> getAllPaths() {
    return (HashMap<String, ArrayList<String>>) dir.clone();
  }

  public void setAllPaths(HashMap<String, ArrayList<String>> paths) {
    dir = paths;
  }
}
