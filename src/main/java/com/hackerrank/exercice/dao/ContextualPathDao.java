package com.hackerrank.exercice.dao;

import java.io.File;
import java.nio.file.Paths;

public class ContextualPathDao {

  String path = System.getProperty("user.dir");

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public void changeToParent() {
    File file = new File(path);
    path = file.getParent();
  }

  public void changeToRoot() {
    File file = new File(path);
    path = Paths.get(file.getPath()).getRoot().toString();
  }
}
