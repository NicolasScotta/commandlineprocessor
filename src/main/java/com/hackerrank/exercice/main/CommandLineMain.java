package com.hackerrank.exercice.main;

import com.hackerrank.exercice.model.core.CommandLine;

public class CommandLineMain {

  public static void main(String[] args) throws InterruptedException {
    CommandLine commandLine = new CommandLine();
    commandLine.start();
  }
}
