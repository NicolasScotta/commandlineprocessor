package com.hackerrank.exercice.model.dto;

import java.util.ArrayList;
import java.util.List;

public class ResultDTO {

  List<String> outputLines = new ArrayList<>();

  boolean success = false;

  boolean shouldPersist = false;

  boolean shouldExit = false;

  boolean changeDir = false;

  boolean parentDir = false;

  boolean root = false;

  boolean file = false;

  public List<String> getOutputLines() {
    return outputLines;
  }

  public ResultDTO setOutputLines(List<String> outputLines) {
    this.outputLines = outputLines;
    return this;
  }

  public boolean isSuccess() {
    return success;
  }

  public ResultDTO setSuccess(boolean success) {
    this.success = success;
    return this;
  }

  public boolean isShouldExit() {
    return shouldExit;
  }


  public boolean isParentDir() {
    return parentDir;
  }

  public boolean isChangeDir() {
    return changeDir;
  }

  public boolean isRoot() {
    return root;
  }

  public ResultDTO setRoot(boolean root) {
    this.root = root;
    return this;
  }

  public ResultDTO setChangeDir(boolean changeDir) {
    this.changeDir = changeDir;
    return this;
  }

  public ResultDTO setParentDir(boolean parentDir) {
    this.parentDir = parentDir;
    return this;
  }

  public boolean isFile() {
    return file;
  }

  public ResultDTO setFile(boolean file) {
    this.file = file;
    return this;
  }

  public ResultDTO setShouldExit(boolean shouldExit) {
    this.shouldExit = shouldExit;
    return this;
  }

  public boolean isShouldPersist() {
    return shouldPersist;
  }

  public ResultDTO setShouldPersist(boolean shouldPersist) {
    this.shouldPersist = shouldPersist;
    return this;
  }

  public ResultDTO addOutPutLine(String line) {
    outputLines.add(line);
    return this;
  }

  public ResultDTO addAllOutPutLine(List<String> lines) {
    outputLines.addAll(lines);
    return this;
  }
}
