package com.hackerrank.exercice.model.core;

import com.hackerrank.exercice.services.BackupService;
import com.hackerrank.exercice.services.OrchestratorService;
import com.hackerrank.exercice.services.RestoreService;
import java.util.Scanner;

public class CommandLine extends Thread {

  /**
   * Objeto de Consola
   */

  @Override
  public void run() {
    try {
      RestoreService.INSTANCE.restoreMemoryPaths();
      Scanner in = new Scanner(System.in);
      OrchestratorService.INSTANCE.writePath();
      while (true) {
        String command = in.nextLine();
        boolean shouldQuit = OrchestratorService.INSTANCE.processInputCommand(command);
        if (shouldQuit) {
          BackupService.INSTANCE.shutdown();
          in.close();
        }
      }
    } catch (IllegalStateException e) {
    }
  }
}
