package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.services.ContextualPathService;
import com.hackerrank.exercice.services.PersistenceService;
import java.io.File;

public class MKDirEvent extends ConsoleEvent {

  public static final String MKDIR_PREFIX = "mkdir";

  @Override
  public ResultDTO processEvent(String line) {
    if (line.startsWith(MKDIR_PREFIX)) {
      if (validateInput(line)) {
        String path = line.replace(MKDIR_PREFIX + " ", "");
        if (existDirectory(ContextualPathService.INSTANCE.getBasePath() + File.separator + path)) {
          return new ResultDTO().setSuccess(false).setShouldPersist(false)
              .addOutPutLine("Directory already exists");
        } else {
          return new ResultDTO().setSuccess(true).setShouldPersist(true)
              .addOutPutLine("Directory created in memory");
        }
      } else {
        return new ResultDTO().setSuccess(false).setShouldPersist(false)
            .addOutPutLine("Invalid Command");
      }
    } else {
      return null;
    }
  }

  private boolean validateInput(String line) {
    if (line.startsWith(MKDIR_PREFIX + " ")) {
      String path = line.replace(MKDIR_PREFIX + " ", "");
      return path.length() < 100 && path.length() != 0;
    }
    return false;
  }

  private boolean existDirectory(String path) {
    return PersistenceService.INSTANCE.checkExistsDirectory(path);
  }
}
