package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.services.PersistenceService;

public class TouchEvent extends ConsoleEvent {

  public static final String TOUCH_PREFIX = "touch";

  @Override
  public ResultDTO processEvent(String line) {
    if (line.startsWith(TOUCH_PREFIX)) {
      if (validateInput(line)) {
        return new ResultDTO().setSuccess(true).setShouldPersist(true).setFile(true)
            .addOutPutLine("File Creation must be implemented");
      } else {
        return new ResultDTO().setSuccess(false).setShouldPersist(false)
            .addOutPutLine("Invalid Command");
      }
    } else {
      return null;
    }
  }

  private boolean validateInput(String line) {
    if (line.startsWith(TOUCH_PREFIX + " ")) {
      String path = line.replace(TOUCH_PREFIX + " ", "");
      return path.length() < 100 && path.length() != 0;
    }
    return false;
  }

  private boolean existDirectory(String path) {
    return PersistenceService.INSTANCE.checkExistsDirectory(path);
  }
}
