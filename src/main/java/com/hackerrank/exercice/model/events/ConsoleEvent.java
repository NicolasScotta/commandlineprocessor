package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;

public abstract class ConsoleEvent {

  public abstract ResultDTO processEvent(String line);
}
