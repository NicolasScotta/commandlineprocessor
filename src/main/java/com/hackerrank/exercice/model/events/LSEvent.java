package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.constants.EventConstants;
import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.services.ContextualPathService;
import com.hackerrank.exercice.services.PersistenceService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LSEvent extends ConsoleEvent {


  @Override
  public ResultDTO processEvent(String line) {
    try {
      if (line.equalsIgnoreCase(EventConstants.LS_R)) {
        List<String> realFiles = getRealFiles(true);
        List<String> memoryFiles = getMemoryFiles(true);
        return new ResultDTO().setSuccess(true).addAllOutPutLine(realFiles)
            .addAllOutPutLine(memoryFiles);
      } else if (line.equalsIgnoreCase(EventConstants.LS)) {
        List<String> realFiles = getRealFiles(false);
        List<String> memoryFiles = getMemoryFiles(false);
        return new ResultDTO().setSuccess(true).addAllOutPutLine(realFiles)
            .addAllOutPutLine(memoryFiles);
      } else {
        return null;
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private List<String> getMemoryFiles(boolean recursively) {
    if (recursively) {
      return PersistenceService.INSTANCE.lsRecursively(
          ContextualPathService.INSTANCE.getBasePath());
    } else {
      return PersistenceService.INSTANCE.ls(
          ContextualPathService.INSTANCE.getBasePath());
    }
  }

  private List<String> getRealFiles(boolean recursively) throws IOException {
    File file = new File(ContextualPathService.INSTANCE.getBasePath());
    if (recursively) {
      return getAllFilesRecursive(file);
    } else {
      return getAllFiles(file);
    }
  }

  private ArrayList<String> getAllFilesRecursive(File file) {
    if (file.isDirectory()) {
      ArrayList<String> a = new ArrayList();
      a.add(file.getAbsolutePath());
      for (String f : file.list()) {
        a.addAll(getAllFilesRecursive(new File(file.getAbsolutePath() + File.separator + f)));
      }
      return a;
    } else {
      return new ArrayList<>(Arrays.asList(file.getAbsolutePath()));
    }
  }

  private ArrayList<String> getAllFiles(File file) {
    ArrayList<String> a = new ArrayList();
    if (file.isDirectory()) {
      for (String f : file.list()) {
        a.add(File.separator + f);
      }
    }
    return a;
  }
}
