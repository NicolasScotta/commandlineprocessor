package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;

public class QuitEvent extends ConsoleEvent {

  @Override
  public ResultDTO processEvent(String line) {
    if (line.equalsIgnoreCase("quit")) {
      return new ResultDTO().setSuccess(true).setShouldExit(true).addOutPutLine("Bye ^^");
    } else {
      return null;
    }
  }
}
