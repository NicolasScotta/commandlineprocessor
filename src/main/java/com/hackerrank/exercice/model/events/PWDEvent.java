package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.services.ContextualPathService;

public class PWDEvent extends ConsoleEvent {

  @Override
  public ResultDTO processEvent(String line) {
    if (line.equalsIgnoreCase("pwd")) {
      return new ResultDTO().setSuccess(true)
          .addOutPutLine(ContextualPathService.INSTANCE.getBasePath());
    } else {
      return null;
    }
  }
}
