package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;

public class UnrecognizedEvent extends ConsoleEvent {

  @Override
  public ResultDTO processEvent(String line) {
    return new ResultDTO().setSuccess(false).addOutPutLine("Unrecognized command");
  }
}
