package com.hackerrank.exercice.model.events;

import com.hackerrank.exercice.model.dto.ResultDTO;
import com.hackerrank.exercice.services.ContextualPathService;
import com.hackerrank.exercice.services.PersistenceService;
import java.io.File;


public class CDEvent extends ConsoleEvent {

  public static final String CD_PREFIX = "cd";

  @Override
  public ResultDTO processEvent(String line) {
    if (line.startsWith(CD_PREFIX)) {
      if (validateInput(line)) {
        String path = line.replace(CD_PREFIX + " ", "");
        if (path.equalsIgnoreCase("..")) {
          return new ResultDTO().setSuccess(true).setShouldPersist(false).setParentDir(true);
        } else if (path.equalsIgnoreCase(File.separator)) {
          return new ResultDTO().setSuccess(true).setShouldPersist(false).setRoot(true);
        } else {
          if (!path.startsWith("C:") && !path.startsWith(File.separator)) {
            path = ContextualPathService.INSTANCE.getBasePath() + File.separator + path;
          }
          if (existDirectory(path)) {
            return new ResultDTO().setSuccess(true).setShouldPersist(false).setChangeDir(true);
          } else {
            //FIXME aca ver si no es un path hibrido (Disk + Memory)
            return new ResultDTO().setSuccess(false).setShouldPersist(false)
                .addOutPutLine(
                    line.contains(File.separator) ? "Invalid path" : "Directory not found");
          }
        }
      } else {
        return new ResultDTO().setSuccess(false).setShouldPersist(false)
            .addOutPutLine("Invalid Command");
      }
    }
    return null;
  }

  private boolean validateInput(String line) {
    return line.startsWith(CD_PREFIX + " ");
  }

  private boolean existDirectory(String path) {
    return PersistenceService.INSTANCE.checkExistsDirectory(path);
  }
}
