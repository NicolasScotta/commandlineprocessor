package com.hackerrank.exercice.model.backup;

import java.util.ArrayList;
import java.util.List;

public class BackupDTO {

  String root;

  List<String> directories = new ArrayList<>();

  public BackupDTO(String root, List<String> directories) {
    this.root = root;
    this.directories = directories;
  }

  public String getRoot() {
    return root;
  }

  public void setRoot(String root) {
    this.root = root;
  }

  public List<String> getDirectories() {
    return directories;
  }

  public void setDirectories(List<String> directories) {
    this.directories = directories;
  }

  public void addDirectory(String directory) {
    this.directories.add(directory);
  }
}
