package com.hackerrank.exercice.services;

import com.hackerrank.exercice.model.dto.ResultDTO;
import org.junit.Assert;
import org.junit.Test;

public class CatalogServiceSuites {

  @Test
  public void testPWD() {
    ResultDTO result = CatalogService.INSTANCE.processLine("pwd");
    Assert.assertEquals(System.getProperty("user.dir"),
        result.getOutputLines().stream().findFirst().get());
    Assert.assertTrue(result.isSuccess());
    Assert.assertFalse(result.isShouldExit());
    Assert.assertFalse(result.isChangeDir());
    Assert.assertFalse(result.isShouldPersist());
  }

  @Test
  public void testLs() {
    ResultDTO result = CatalogService.INSTANCE.processLine("ls");
    Assert.assertFalse(result.getOutputLines().isEmpty());
    Assert.assertTrue(result.isSuccess());
    Assert.assertFalse(result.isShouldExit());
    Assert.assertFalse(result.isChangeDir());
    Assert.assertFalse(result.isShouldPersist());
  }

  @Test
  public void testMKDir() {
    ResultDTO result = CatalogService.INSTANCE.processLine("mkdir abc");
    Assert.assertFalse(result.getOutputLines().isEmpty());
    Assert.assertTrue(result.isSuccess());
    Assert.assertFalse(result.isShouldExit());
    Assert.assertFalse(result.isChangeDir());
    Assert.assertTrue(result.isShouldPersist());
  }
}
