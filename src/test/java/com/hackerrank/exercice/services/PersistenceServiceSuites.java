package com.hackerrank.exercice.services;

import org.junit.Assert;
import org.junit.Test;

public class PersistenceServiceSuites {

  @Test
  public void testPersist() {
    Assert.assertFalse(PersistenceService.INSTANCE.checkExistsDirectory("abc"));

    PersistenceService.INSTANCE.persist(System.getProperty("user.dir"), "mkdir abc",
        CatalogService.INSTANCE.processLine("mkdir abc"));

    Assert.assertTrue(PersistenceService.INSTANCE.checkExistsDirectory("abc"));

  }
}
